package no.ntnu.idatt2105.l4.demo.service;

import no.ntnu.idatt2105.l4.demo.model.Meme;
import no.ntnu.idatt2105.l4.demo.repo.Lesson4Repo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.lenient;

/* Klasse som viser hvordan mocking fungerer. Legg merke til det som skjer  i metoden setUp(),
* og da spesielt linja "Mockite.lenient().when(repo.saySomething()).thenReturn(needForCreate).
* Denne linja viser hvordan man bare returnerer Meme-instansen 'neededForCreate' når man kaller
* repo.saySomething().
*
* På linje under har vi "lenient().doNothing().when(repo).create(needForCreate)", og denne sier
* at når vi kaller metoden create() som ligger i Lesson4Repo, så skal vi ikke gjøre noen verdens
* ting. Grunnen til at vi har med en instans av Meme her (neededForCreate), er at metode-signaturen
* krever det. Vi ser et eksempel på hvordan dette fungerer i metoden *testen* create(), hvor vi
* kaller create()-metoden, men sender inn et Meme som har bare tomme strenger som 'memeText' og
* 'pic'. Dette er i den reelle implementasjonen ikke lov, og vil kaste et IllegalArgumentException,
* men testen passerer i dette tilfellet fordi vi kaller create() på mock-objektet i stedet, og
* som allerede sagt, så skal vi gjøre ingenting (doNothing()) når det skjer.
*
* */

@ExtendWith(MockitoExtension.class)
public class Lesson4ServiceTest {

    @InjectMocks
    private Lesson4Service service;

    @Mock
    private Lesson4Repo repo;

    @BeforeEach // NB!! Mock-konteksten resettes mellom hver testmetode (@AfterEach kjøres og rydder opp), så bare @Before/@BeforeClass vil ikke funke
    public void setUp() {
        Meme neededForCreate = new Meme("fake memeText", "fake memePic");

        // merk bruk av lenient() her, ellers blir det exception
        //Se https://www.baeldung.com/mockito-unnecessary-stubbing-exception
        Mockito.lenient().when(repo.saySomething()).thenReturn(neededForCreate);
        lenient().doNothing().when(repo).create(neededForCreate);
    }

    @Test
    void create() {
        try {
            repo.create(new Meme("", ""));
        } catch (IllegalArgumentException iae) {
            System.out.println("We should never have reached this point.");
            System.out.println("Did we somehow call the real method in stead of the mocked one?");
            fail();
        }
    }

    @Test
    void lesson4Message() {
        List<Meme> memes = service.lesson4Message();

        assertThat(memes.get(0).getMemeText()).isEqualTo("fake memeText");
        assertThat(memes.get(0).getPic()).isEqualTo("fake memePic");
    }
}