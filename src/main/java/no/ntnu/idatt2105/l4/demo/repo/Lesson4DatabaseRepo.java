package no.ntnu.idatt2105.l4.demo.repo;

import no.ntnu.idatt2105.l4.demo.model.Meme;
import no.ntnu.idatt2105.l4.demo.web.Lesson4Controller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Profile("!nodb") // Use for any profile that isn't "nodb"
@Repository
public class Lesson4DatabaseRepo implements Lesson4Repo {
    Logger logger = LoggerFactory.getLogger(Lesson4Repo.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public Meme saySomething() {
        // Databasen inneholder én tabell (info), som har én kolonne (infostring), som har én rad (string som beskriver hvordan base (dev eller test) det er).
        // Se også i application-MILJØNAVN.properties for hvordan man angir credentials for å koble seg til DB.
        // I den sammenhengen, vær spesielt oppmerksom på 'spring.datasource.url', som angir connection-string for å koble til databasen.
        // Det som er litt spesielt med 'spring.datasource.url' er at den også angir hvilken type database man går mot, slik at Spring Boot
        // kan velge riktig driver. I dette eksempelet er PostgreSQL brukt, men om man f.eks. skal bruke MySQL vil denne stringen ha et annet format.
        // Søk på nett for rett format på connection-string for databasen du tenker å bruke.

        String result = jdbcTemplate.queryForList("SELECT infostring FROM info", String.class).get(0);
        // queryForList() er én av en del forskjellige metoder man kan bruke for å spørre mot databasen.
        // En annen kan være queryForObject(), men denne krever en såkalt RowMapper, som er en klasse som
        // implementerer interfacet RowMapper. Dette interfacet har én metode, mapRow(), som utfører mapping
        // for hver rad (dvs. metoden blir kjørt én gang for hver rad i result-settet) til Java-objekter.
        // Se https://github.com/eugenp/tutorials/blob/master/persistence-modules/spring-jdbc/src/main/java/com/baeldung/spring/jdbc/template/guide/EmployeeRowMapper.java
        // for eksempel.
        // For å inserte kan en bruke execeute(). Se https://github.com/eugenp/tutorials/blob/master/persistence-modules/spring-jdbc/src/main/java/com/baeldung/spring/jdbc/template/guide/EmployeeDAO.java
        // men legg merke til at dette eksempelet benytter mer enn bare JdbcTemplate (SimpleJdbcInsert, SimpleJdbcCall, f.eks.), som er noe vi ikke
        // bryr oss om. Du står dog fritt til å bruke om du vil.

        return new Meme("Meme hentet fra base", result);
    }

    public void create(Meme meme) {
        if (meme.getMemeText().isEmpty() || meme.getPic().isEmpty()) throw new IllegalArgumentException("Must provide non-empty text for both text and pic");

        logger.debug("Creating new meme...");
    }
}
