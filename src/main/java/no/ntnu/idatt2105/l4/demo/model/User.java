package no.ntnu.idatt2105.l4.demo.model;

/**
 * Class representing a generic user, which can probably be used directly in most systems
 * (as long as the e-mail address is used as username). Though, this class can easily be
 * expanded to cover all the attributes you'd need for a user.
 *
 */
public class User {

    private Integer userid;

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    private String username;

    public User(Integer userid, String username) {
        this.userid = userid;
        this.username = username;
    }

    @Override
    public String toString() {
        return "User [userid=" + userid + ", username=" + username + "]";
    }
}