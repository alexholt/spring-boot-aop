package no.ntnu.idatt2105.l4.demo.service;

import no.ntnu.idatt2105.l4.demo.model.User;

import java.util.List;

public interface UserService {

    List<User> getAllUsers();

    User getUser(Integer userid);

    void createUser(Integer userid, String username);

    void updateUser(Integer userid, String username);

    void deleteUser(Integer userid);

    boolean isValid(String username, String password);
}