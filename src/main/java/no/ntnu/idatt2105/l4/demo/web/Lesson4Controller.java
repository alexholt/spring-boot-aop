package no.ntnu.idatt2105.l4.demo.web;

import no.ntnu.idatt2105.l4.demo.aop.TokenRequired;
import no.ntnu.idatt2105.l4.demo.model.Meme;
import no.ntnu.idatt2105.l4.demo.service.Lesson4Service;
import no.ntnu.idatt2105.l4.demo.service.SecurityService;
import no.ntnu.idatt2105.l4.demo.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.security.Key;
import java.util.*;

@CrossOrigin()
@RestController
public class Lesson4Controller {

    @Autowired
    private Lesson4Service service;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserService userService;

    Logger logger = LoggerFactory.getLogger(Lesson4Controller.class);

    @GetMapping("/open")
    public List<Meme> openEndpoint() {
        System.out.println("request til /open motatt");
        return this.service.lesson4Message();
    }

    @TokenRequired
    @GetMapping("/restricted")
    public List<Meme> restrictedEndpoint() {
        System.out.println("request til /restricted mottatt");
        return this.service.lesson4Message();
    }

    @TokenRequired
    @GetMapping("/colors")
    public List<String> restrictedColors() {
        System.out.println("request til /colors mottatt");
        return Arrays.asList("Blue", "Green", "Our Red", "Yellow");
    }

    @ResponseBody
    @RequestMapping("/security/generate/token")
    public Map<String, Object> generateToken(@RequestParam(value="subject") String subject){

        String token = securityService.createToken(subject, (150 * 1000 * 60));

        Map<String, Object> map = new LinkedHashMap<>();
        map.put("result", token);

        return map;
    }

    // Intended to show a possible way of adding the JWT to the header of the response, in stead of just returning it in a map like in generateToken()
    @ResponseBody
    @PostMapping("/login")
    public ResponseEntity<Object> login(@RequestBody String username, @RequestBody String password) {
        System.out.println("request til login mottatt");
        if (userService.isValid(username, password)) {
            String token = "Bearer "+ securityService.createToken(username, 10*100);
            MultiValueMap<String, String> headers = new HttpHeaders(); //HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", token); // JWT should be in the header of the HTTP response
            headers.add("Access-Control-Expose-Headers", "Authorization");

            System.out.println("returnerer response-entity");
            return new ResponseEntity<Object>(headers, HttpStatus.OK);
        } else
        {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Not a valid username/password combo."); // will never occur, since isValid() only returns true
        }
    }
}
