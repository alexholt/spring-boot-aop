package no.ntnu.idatt2105.l4.demo.repo;

import no.ntnu.idatt2105.l4.demo.model.Meme;

// Her har vi innført et interface, som gjør at vi kan bruke forskjellige implementasjoner (Lesson4DatabaseRepo og Lesson4NoDatabaseRepo)
// vha. dependency injection. Blir vanskelig å bruke profiler uten å gjøre det på denne måten.
public interface Lesson4Repo {

    public Meme saySomething();

    public void create(Meme meme);
}
