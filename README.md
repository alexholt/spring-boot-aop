# Example application for Spring Boot

Intended mostly to show off how Gitlab CI works with a Spring Boot/Maven project.

The CI will run tests and then make a JAR of the Spring Boot application available as an artifact.
Additionally, JUnit tests can be seen on for each run of a pipeline, and Jacoco (code coverage reports) are made available on Gitlab Pages (Settings -> Pages, in the menu on the left).

The project also show off how to use an in-mem database (H2), which is only utilized when running the 'gitlab-ci' Spring Profile, as well as a very basic initialization/population of said DB. Other profiles are present as well (see src/main/resources), among them, an example of having a repo which simply returns hard coded values in stead of fetching something from the database. An example of mocking with Mockito is also present. 

The code is fairly well commented, and may be of help as a very simple example of how things can be done. Everything is based off of code shown during a lecture.

